const { awsConfig, originalBucket } = require('../config');
const S3Zipper = require('aws-s3-zipper');
const zipper = new S3Zipper({
  accessKeyId: awsConfig.accessKeyId,
  secretAccessKey: awsConfig.secretAccessKey,
  region: awsConfig.region,
  bucket: originalBucket,
});

class ZipMaker {
  /**
   * makeZip method is used to make a zip folder of images
   * @param  {[type]}  category [description]
   * @return {Promise}          [description]
   */
  makeZip(category, subCategory) {
    return new Promise((resolve, reject) => {
      zipper.zipToFile(
        {
          s3FolderName: `${category}/${subCategory}`,
          zipFileName: `${__dirname}/../public/${category}-${subCategory}.zip`,
          recursive: true,
        },
        (err, result) => {
          if (err) {
            console.error(err);
            reject(err);
          } else {
            console.log('DONE - ', category, subCategory);
            resolve(result);
          }
        },
      );
    });
  }

  /**
   * makeZip method is used to make a zip folder of images
   * @param  {[type]}  category [description]
   * @return {Promise}          [description]
   */
  makeZipOfCategory(category) {
    return new Promise((resolve, reject) => {
      zipper.zipToFile(
        {
          s3FolderName: `${category}`,
          zipFileName: `${__dirname}/../public/${category}.zip`,
          recursive: true,
        },
        (err, result) => {
          if (err) {
            console.error(err);
            reject(err);
          } else {
            console.log('DONE - ', category);
            resolve(result);
          }
        },
      );
    });
  }
}

module.exports = new ZipMaker();
