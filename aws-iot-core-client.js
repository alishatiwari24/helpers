const awsIot = require('aws-iot-device-sdk');

// Replace these with your file paths
const device = awsIot.device({
  keyPath:
    './843e73b0134853ed05b876c28227b75760bf258aa0165342559d33cd820368dd-private.pem.key', // Path to the private key
  certPath:
    './843e73b0134853ed05b876c28227b75760bf258aa0165342559d33cd820368dd-certificate.pem.crt', // Path to the device certificate
  caPath:
    './843e73b0134853ed05b876c28227b75760bf258aa0165342559d33cd820368dd-public.pem.key', // Path to the Root CA certificate
  clientId: 'iotconsole-d7689c27-8e45-4f9c-b74a-dee1e1290031', // Unique client ID
  host: 'a1hvdsbtd6oi8q-ats.iot.us-east-1.amazonaws.com', // Your AWS IoT Core endpoint
});

// Connect to AWS IoT Core
device.on('connect', () => {
  console.log('Connected to AWS IoT Core');

  // Subscribe to a topic
  device.subscribe('test/topic', () => {
    console.log('Subscribed to test/topic');
  });

  // Publish a message to the topic
  device.publish(
    'test/topic',
    JSON.stringify({ message: 'Hello from Node.js!' }),
  );
});

// Handle incoming messages
device.on('message', (topic, payload) => {
  console.log(`Message received on topic ${topic}:`, payload.toString());
});

// Error handling
device.on('error', (error) => {
  console.error('Error:', error);
});
