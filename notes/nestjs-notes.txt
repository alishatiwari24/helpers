nest new projectname                            - To create a nest project

nest g module modulename                        - To create a module

nest g controller controllername                - To create a controller
nest g controller controllername --no-spec      - To create controller without spec.ts file

nest g service servicename 			- To create a service

*.model.ts                    - These files created by user manually and contains interfaces or types

npm install class-validator class-transformer   - To install modules for validation using ValidationPipe

