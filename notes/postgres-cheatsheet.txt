SequelizeError: Sorry, too many clients already 

 solution:- 
    1. sudo -u postgres psql
    2. SHOW max_connections;
    3. SHOW config_file;
    4. Open the config file and edit "max_connection" property value
    5. Restart postgres service using sudo systemctl
    6. Check max connections using (2)

To change Postgres password

 commands:-
    1. sudo -u postgres psql
    2. ALTER USER postgres PASSWORD 'newpassword';
 
Update JSON doc in postgres:

UPDATE public.nursedocuments
	SET sp_documents=jsonb_set(sp_documents, '{sp_driverslicense.sp_isadminverified}', 'true')
	WHERE sp_documentid='bd483a47-83a9-49d0-a32e-5b834561fe6a';

Remove whole & install again
   - If you really need to do a full purge and reinstall, first make sure PostgreSQL isn't running. ps -C postgres should show no     results.

Now run:

apt-get --purge remove postgresql\*
to remove everything PostgreSQL from your system. Just purging the postgres package isn't enough since it's just an empty meta-package.

Once all PostgreSQL packages have been removed, run:

rm -r /etc/postgresql/
rm -r /etc/postgresql-common/
rm -r /var/lib/postgresql/
userdel -r postgres
groupdel postgres
You should now be able to:

apt-get install postgresql
 
