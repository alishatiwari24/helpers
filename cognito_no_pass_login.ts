import {
  CognitoIdentityProviderClient,
  InitiateAuthCommand,
  SignUpCommand,
} from '@aws-sdk/client-cognito-identity-provider';

// Initialize Cognito Identity Provider client
const client = new CognitoIdentityProviderClient({
  region: 'us-east-1',
  credentials: {
    accessKeyId: 'AKIA47CR3H6PCEFU475G',
    secretAccessKey: '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',
  },
});

// User authentication function
const login = async () => {
  try {
    const command = new InitiateAuthCommand({
      AuthFlow: 'USER_PASSWORD_AUTH',
      ClientId: '47d94jjkld3f72c5t9q2prj0ss',
      AuthParameters: {
        USERNAME: 'alisha.t@simformsolutions.com',
        PASSWORD: 'Test@123',
      },
    });
    const response = await client.send(command);
    console.log('Login successful:', response);
  } catch (err) {
    console.error('Error during login:', err.message);
  }
};

const signUpUser = async () => {
  const params = {
    ClientId: '47d94jjkld3f72c5t9q2prj0ss', // Your Cognito User Pool App Client ID
    Username: 'AlishaUser', // User's username
    Password: 'Test@123', // User's password
    UserAttributes: [
      {
        Name: 'email', // User attribute
        Value: 'alisha.t@simformsolutions.com',
      },
    ],
  };

  try {
    const command = new SignUpCommand(params);
    const response = await client.send(command);
    console.log('Sign-up successful!', response);
  } catch (error) {
    console.error('Error during sign-up:', error.message);
  }
};

login();
