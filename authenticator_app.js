const express = require('express');
const { authenticator } = require('otplib');
const QRCode = require('qrcode');

const app = express();
app.use(express.json());

// Mock database for storing user secrets
const userDatabase = {};

// Endpoint to generate 2FA QR code
app.post('/generate-2fa', (req, res) => {
  const { email } = req.body;
  const secret = authenticator.generateSecret();
  userDatabase[email] = { secret }; // Store secret in database

  const appName = 'MyApp';
  const otpauth = authenticator.keyuri(email, appName, secret);

  QRCode.toDataURL(otpauth, (err, url) => {
    if (err) {
      return res.status(500).send('Error generating QR code');
    }
    res.send({ qrCode: url });
  });
});

// Endpoint to verify 2FA token
app.post('/verify-2fa', (req, res) => {
  const { email, token } = req.body;
  const user = userDatabase[email];

  if (!user) return res.status(404).send('User not found');

  const isValid = authenticator.verify({ token, secret: user.secret });
  if (isValid) {
    res.send('2FA Verified Successfully');
  } else {
    res.status(401).send('Invalid 2FA Token');
  }
});

app.listen(3000, () => {
  console.log('Server is running on http://localhost:3000');
});
