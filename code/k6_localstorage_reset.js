import http from 'k6/http';

export default function () {
  // Replace 'https://example.com' with the URL of the website where you want to clear local storage
  const url = 'https:dev-fe.realbricks.com';

  // Send an HTTP request to the target URL to execute the JavaScript to clear local storage
  const response = http.get(url);

  if (response.status === 200) {
    console.log(`Local storage cleared successfully for ${url}`);
  } else {
    console.error(
      `Failed to clear local storage for ${url}. Status: ${response.status}`,
    );
  }
}
