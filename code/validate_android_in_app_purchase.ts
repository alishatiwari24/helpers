import * as gapi from 'googleapis';
import * as key from '../config/socialgames-b8b90e849100.json';
import { BadRequestException } from '@nestjs/common';

const getGooglePlayTransaction = async (request: {
  applicationPackageName: string;
  productId: string;
  purchaseToken: string;
}) => {
  const oauth2Client = new gapi.Auth.JWT(
    key.client_email,
    '../config/socialgames-b8b90e849100.json',
    key.private_key,
    ['https://www.googleapis.com/auth/androidpublisher'],
    '',
    key.private_key_id,
  );
  await oauth2Client.authorize();
  return await oauth2Client
    .request({
      url: `https://androidpublisher.googleapis.com/androidpublisher/v3/applications/${request.applicationPackageName}/purchases/products/${request.productId}/tokens/${request.purchaseToken}`,
    })
    .then((res) => {
      console.log(res, 'res');
      return res as any;
    })
    .catch((e) => {
      console.log(e, 'Error in googlePlayTransaction');
      throw new BadRequestException('Transaction not found');
    });
};
