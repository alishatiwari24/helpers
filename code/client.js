// const socket = require('socket.io-client')('http://websocket-1045228123.eu-central-1.elb.amazonaws.com', {
// const socket = require('socket.io-client')('https://update.socialgames365.com/?access_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiYWRtaW5Ac29jaWFsZ2FtZXMuY29tIiwiaWF0IjoxNjExMDQ4MTc5LCJleHAiOjE2MTM2NDAxNzl9.WuUfLoZOuiLhyddc_ns2xt0SDP3-vq6P8vlztR_JQRE', {
const socket = require('socket.io-client')(
  'https://update.socialgames365.com?access_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiYWRtaW5Ac29jaWFsZ2FtZXMuY29tIiwiaWF0IjoxNjExMDQ5NzI4fQ.KToXrqoecJ1qTBRS49nfrzxO8rpYTQ0_Hf58k90x80c',
  {
    transports: ['websocket'],
  },
);
socket.on('connect', () => {
  console.log('connected');
});
socket.on('data', (data) => {
  console.log(data);
});
socket.on('daily-quest', (data) => {
  console.log(data);
});
socket.on('disconnect', () => {
  console.log('disconnected');
});
socket.on('error', (err) => {
  console.log('error', err);
});
socket.on('connect_timeout', (err) => {
  console.log('connect_timeout', err);
});
socket.on('reconnect_error', (err) => {
  console.log('reconnect_error', err);
});
socket.on('reconnect_attempt', (err) => {
  console.log('reconnect_attempt', err);
});
