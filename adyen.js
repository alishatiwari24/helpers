const axios = require('axios');

const adyenApiKey =
  'AQElhmfuXNWTK0Qc+iSDm2k+qPaWGd8fJmhCoqthzOWc80WyypZWeBDBXVsNvuR83LVYjEgiTGAH-cKurK3VgTRWaIHTvCa3HqjNdwxOkTbYqAA2IdymehU8=-i1ij52cCM:&th2JBC]}';
const adyenBaseUrl =
  'https://cal-test.adyen.com/cal/services/Account/v6/createAccountHolder';

// Onboarding a fundraiser (e.g., a nonprofit or individual)
async function createFundraiser() {
  const payload = {
    accountHolderCode: 'ws@Company.Simform142',
    accountHolderDetails: {
      individualDetails: {
        name: {
          firstName: 'Jane',
          lastName: 'Doe',
        },
        address: {
          country: 'US',
          city: 'New York',
          postalCode: '10001',
          street: 'Fundraiser St',
          houseNumberOrName: '123',
        },
        email: 'alishasimform@gmail.com',
      },
      bankAccountDetails: [
        {
          countryCode: 'US',
          currencyCode: 'USD',
          accountNumber: '987654321',
          bankCode: '123456789',
        },
      ],
    },
    legalEntity: 'Individual', // Can also be an organization
    processingTier: 1,
  };

  try {
    const response = await axios.post(`${adyenBaseUrl}`, payload, {
      headers: {
        'X-API-Key': adyenApiKey,
        'Content-Type': 'application/json',
      },
    });
    console.log('Fundraiser onboarded:', response.data);
  } catch (error) {
    console.log(error);

    console.error(
      'Error onboarding fundraiser:',
      error.response?.data || error.message,
    );
  }
}

createFundraiser().then((res) => console.log(res, 'success'));

// const adyenApiKey =
//   'AQElhmfuXNWTK0Qc+iSDm2k+qPaWGd8fJmhCoqthzOWc80WyypZWeBDBXVsNvuR83LVYjEgiTGAH-cKurK3VgTRWaIHTvCa3HqjNdwxOkTbYqAA2IdymehU8=-i1ij52cCM:&th2JBC]}';
// const adyenBaseUrl = 'https://checkout-test.adyen.com/v68/customers'; // Adyen test environment URL

// Function to create a new customer
async function createCustomer() {
  const payload = {
    email: 'john.doe@example.com', // Customer's email
    firstName: 'John', // Customer's first name
    lastName: 'Doe', // Customer's last name
    phone: '+1234567890', // Customer's phone number (optional)
    country: 'US', // Customer's country (optional)
    // Add any other relevant customer details
  };

  try {
    const response = await axios.post(adyenBaseUrl, payload, {
      headers: {
        'X-API-Key': adyenApiKey,
        'Content-Type': 'application/json',
      },
    });
    console.log('Customer created:', response.data);
  } catch (error) {
    console.error(
      'Error creating customer:',
      error.response?.data || error.message,
    );
  }
}

// Call the function to create the customer
// createCustomer();
