import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import 'reflect-metadata';
import { createConnection } from 'typeorm';
import { User } from './db/entity/User';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
bootstrap();

export default createConnection({
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'sellnews',
  database: 'DemoDatabase',
  synchronize: true,
  logging: false,
  entities: [User],
})
  .then((connection) => {
    // console.log('connection', connection);
  })
  .catch((error) => {
    console.log('error', error);
  });
