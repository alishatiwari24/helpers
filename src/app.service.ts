import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(name: string): string {
    return `Hello ${name}`;
  }

  concate(var1: any, var2: any): any {
    return var1 + var2;
  }
}
