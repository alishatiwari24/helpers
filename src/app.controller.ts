import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello('World!');
  }

  @Get('concat')
  concate(): any {
    return this.appService.concate(10, 20);
  }
}
