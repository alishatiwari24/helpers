import { Controller, Get } from '@nestjs/common';
import { AppService } from '../app.service';

@Controller('auth')
export class AuthController {
  constructor(private appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello('Auth');
  }
}
