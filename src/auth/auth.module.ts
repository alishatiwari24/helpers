import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
// import { AppService } from 'src/app.service';

// @Global()
@Module({
  // imports: [AppService, AppModule],
  // exports: [AuthModule],
  controllers: [AuthController],
  // providers: [AppService],
})
export class AuthModule {}
