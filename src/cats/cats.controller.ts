import { Controller, Get, Post, Body, Res, HttpStatus } from '@nestjs/common';
import { AppService } from '../app.service';
import * as express from 'express';

@Controller('cats')
export class CatsController {
  constructor(private appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello('Cats');
  }

  @Get('merge')
  concate(): any {
    return this.appService.concate('Alisha', 'Tiwari');
  }

  @Post('identity')
  getIdentity(@Body() data: { name: string }, @Res() res: express.Response) {
    if (data.name && data.name === 'Alisha') {
      // return 'Welcome Admin';
      res.status(HttpStatus.OK).send('Welcome Admin');
    } else {
      res.status(HttpStatus.OK).send(`Welcome User ${data.name}`);
    }
  }
}
