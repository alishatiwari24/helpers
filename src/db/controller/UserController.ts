import { getRepository } from 'typeorm';
import { NextFunction, Request, Response } from 'express';
import { User } from '../entity/User';
import { Controller, Get, Post, Delete, Body, Param } from '@nestjs/common';

@Controller('users')
export class UserController {
  // private userRepository = getRepository(User);

  @Get()
  async all(request: Request, response: Response, next: NextFunction) {
    return getRepository(User).find();
  }

  @Get(':id')
  async one(@Param() params: string, response: Response, next: NextFunction) {
    return getRepository(User).findOne(params);
  }

  @Post()
  async save(
    @Body()
    body: { id: number; firstName: string; lastName: string; age: number },
    response: Response,
    next: NextFunction,
  ) {
    return getRepository(User).save(body);
  }

  @Delete(':id')
  async remove(@Param() id: string, response: Response, next: NextFunction) {
    const userToRemove = await getRepository(User).findOne(id);
    await getRepository(User).remove(userToRemove);
  }
}
