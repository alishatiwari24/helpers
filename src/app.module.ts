import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsController } from './cats/cats.controller';
import { AuthController } from './auth/auth.controller';
import { UserController } from './db/controller/UserController';
// import { DbModule } from './db/db.module';
// import * as TypeOrmModule from '@nestjs/typeorm';
// import { User } from './db/entity/User';

@Module({
  // imports: [
  //   TypeOrmModule.TypeOrmModule.forRoot({
  //     type: 'postgres',
  //     host: 'localhost',
  //     port: 5432,
  //     username: 'postgres',
  //     password: 'sellnews',
  //     database: 'DemoDatabase',
  //     synchronize: true,
  //     logging: false,
  //     entities: [
  //       User,
  //     ],
  //   }),
  //   DbModule,
  // ],
  controllers: [AppController, CatsController, AuthController, UserController],
  providers: [AppService],
  // exports: [AppService],
  // imports: [DbModule],
})
export class AppModule {}
