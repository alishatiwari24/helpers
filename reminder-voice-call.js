const AWS = require('aws-sdk');
var connect = new AWS.Connect();
// main entry to the flow

exports.handler = (event, context, callback) => {
  //define parameter values to use in initiating the outbound call

  var params = {
    ContactFlowId: '79e934f4-90dc-467a-95f5-16652fbd6346',
    DestinationPhoneNumber: '+13213233995',
    InstanceId: 'b593ff61-326a-42f4-9289-dc3b3f8dec25',
    QueueId: '7793ed7a-7b17-4ce3-aa56-b06e1b538da5',
    // Attributes: {"Name": "MyAttribute"},
    SourcePhoneNumber: '+15049108723',
    TrafficType: 'GENERAL',
  };

  // method used to initiate the outbound call from Amazon Connect
  connect.startOutboundVoiceContact(params, function (err, data) {
    console.log('startOutboundVoiceContact initiated!!!');
    if (err) console.log(err, err.stack);
    else console.log(data);
  });

  callback(null, event);
};
