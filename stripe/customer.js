// import Stripe from 'stripe';
// const stripeClient = new Stripe(
//   'sk_test_51LxmqhGo8Gg36btjLMQ2XUdiMVl8OgLcch8iAOSNdkS89RnBFVpS8CzoP8uVW09cvh4YGk98D8NC9GwgXi69EW7700KkJDhOPQ',
//   { apiVersion: '2022-08-01', timeout: 50000 },
// );
const stripeClient = require('stripe')(
  'sk_test_51LxmqhGo8Gg36btjLMQ2XUdiMVl8OgLcch8iAOSNdkS89RnBFVpS8CzoP8uVW09cvh4YGk98D8NC9GwgXi69EW7700KkJDhOPQ',
);
const createCustomer = async () => {
  const customer = await stripeClient.customers.create({
    name: 'Alisha Customer',
    email: 'alisha@gmail.com',
    phone: '5467893421',
    description:
      'My First Test Customer (created for API docs at https://www.stripe.com/docs/api)',
  });
  console.log(customer);
};

const transferToWallet = async () => {
  const paymentIntent = await stripeClient.paymentIntents.create({
    amount: 1000,
    currency: 'usd',
    // confirm: true,
    // customer: 'cus_MhBExXSHNgltuQ',
    automatic_payment_methods: {
      enabled: true,
    },
    // return_url: 'http://localhost:8000/api/health',
  });
  console.log(paymentIntent, 'pi');
};

const createPrice = async () => {
  const price = await stripeClient.prices.create({
    currency: 'usd',
    unit_amount: 1000,
    product: 'prod_Mj9T4hn6HlLFEq',
  });
  console.log(price);
};

const createPaymentLink = async () => {
  const paymentLink = await stripeClient.paymentLinks.create({
    line_items: [{ price: 'price_1Lzi4TGo8Gg36btjWrrH0goX', quantity: 1 }],
  });
  console.log(paymentLink);
};

// createCustomer();
// transferToWallet();
// createPrice();
createPaymentLink();
